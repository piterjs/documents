# Документы
- [Докладчикам](https://github.com/piterjs/documents/blob/master/for_speakers.md)
- [Площадкам](https://github.com/piterjs/documents/blob/master/for_places.md)
- [jit.si, как записывать доклад](https://github.com/piterjs/documents/blob/master/jitsi.md)
